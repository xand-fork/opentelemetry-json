use opentelemetry::trace::TracerProvider;
use opentelemetry::{sdk, sdk::export::trace::SpanExporter};

/// Pipeline builder
#[derive(Debug)]
pub struct Builder {
    trace_config: Option<sdk::trace::Config>,
    provider_builder: opentelemetry::sdk::trace::Builder,
}

impl Default for Builder {
    /// Return the default pipeline builder.
    fn default() -> Self {
        Self {
            trace_config: None,
            provider_builder: sdk::trace::TracerProvider::builder(),
        }
    }
}

impl Builder {
    #[must_use]
    pub fn add_simple_exporter<SE: SpanExporter + 'static>(self, exporter: SE) -> Self {
        let provider_builder = self.provider_builder.with_simple_exporter(exporter);

        Self {
            trace_config: self.trace_config,
            provider_builder,
        }
    }

    #[must_use]
    pub fn add_batch_exporter<SE: SpanExporter + 'static>(self, exporter: SE) -> Self {
        let runtime = opentelemetry::runtime::Tokio;
        let provider_builder = self.provider_builder.with_batch_exporter(exporter, runtime);

        Self {
            trace_config: self.trace_config,
            provider_builder,
        }
    }

    /// Install the exporter pipeline with the recommended defaults.
    /// Copied with mods from [here](https://gitlab.com/TransparentIncDevelopment/r-d/opentelemetry-rust/blob/bd72d1070a22f3c146f29b13502b9d69b2585627/opentelemetry/src/sdk/export/trace/stdout.rs#L95)
    #[must_use]
    pub fn install(mut self) -> sdk::trace::Tracer {
        let mut provider_builder = self.provider_builder;
        if let Some(config) = self.trace_config.take() {
            provider_builder = provider_builder.with_config(config);
        }
        let provider = provider_builder.build();
        // Passing empty string and None will use sdk defaults.
        // See: https://gitlab.com/TransparentIncDevelopment/r-d/opentelemetry-rust/blob/bd72d1070a22f3c146f29b13502b9d69b2585627/opentelemetry/src/sdk/trace/provider.rs#L81
        let tracer = provider.tracer("", None);
        opentelemetry::global::set_tracer_provider(provider);

        tracer
    }
}
