use std::time::Duration;
use tracing_bunyan_formatter::{BunyanFormattingLayer, JsonStorageLayer};
use tracing_subscriber::fmt::format::FmtSpan;
use tracing_subscriber::layer::SubscriberExt;
use tracing_subscriber::util::SubscriberInitExt;

#[tokio::main(flavor = "multi_thread")]
async fn main() {
    // Set up target file
    let trace_data_dir = std::env::current_dir().unwrap().join("traces");
    std::fs::create_dir_all(&trace_data_dir).expect("Error while trying to create create trace data folder");
    let tracefile_prefix = trace_data_dir.join("traces");
    println!("Writing trace data to: {:?}", tracefile_prefix);

    // Build bunyan layer
    let rolling_appender = rolling_file::BasicRollingFileAppender::new(
        tracefile_prefix,
        rolling_file::RollingConditionBasic::new().daily(),
        9,
    )
    .unwrap();
    let (non_blocking_appender, _guard) = tracing_appender::non_blocking(rolling_appender);
    let bunyan_formatting_layer = BunyanFormattingLayer::new("tracing_demo".into(), non_blocking_appender);

    // Set up stdout layer
    let stdout_formatted_layer = tracing_subscriber::fmt::layer()
        // .json() <-- @NATE, this is kinda cool. Not super easy to read, and I don't know
        .with_writer(std::io::stdout)
        .with_span_events(FmtSpan::NONE);

    // Configure and register global subscriber
    tracing_subscriber::Registry::default()
        .with(stdout_formatted_layer)
        .with(JsonStorageLayer)
        .with(bunyan_formatting_layer)
        .try_init()
        .expect("Failed initializing global subscriber");

    trace_me().await;
}

#[tracing::instrument(fields(my_field = 999))]
async fn trace_me() {
    for i in 0..2 {
        do_work(i).await;
    }
}

#[tracing::instrument]
async fn do_work(arg: usize) {
    tracing::info!(iteration = arg);
    if arg == 1 {
        println!("sleeping...");
        tokio::time::sleep(Duration::from_secs(2)).await;
    }
}
