use std::fs::File;
use std::time::Duration;
use tracing_subscriber::fmt::format::FmtSpan;
use tracing_subscriber::layer::SubscriberExt;
use tracing_subscriber::util::SubscriberInitExt;

#[tokio::main(flavor = "multi_thread")]
async fn main() {
    // Set up target file
    let cwd = std::env::current_dir().unwrap();
    let target_file = cwd.join("sample-trace-data.jsonl");
    let my_file = File::create(target_file).unwrap();
    println!("Writing trace data to: {:?}", my_file);

    // Build json_exporter with writer
    let json_exporter = opentelemetry_json::exporter::json::Exporter::new(my_file);

    // Build tracer provider using pipeline::Builder
    let tracer = opentelemetry_json::pipeline::Builder::default()
        .add_batch_exporter(json_exporter)
        .install();
    let json_file_layer = tracing_opentelemetry::layer().with_tracer(tracer);

    // Set up stdout layer
    let stdout_formatted_layer = tracing_subscriber::fmt::layer()
        .with_writer(std::io::stdout)
        .with_span_events(FmtSpan::FULL);

    // Configure and register global subscriber
    tracing_subscriber::Registry::default()
        .with(stdout_formatted_layer)
        .with(json_file_layer)
        .try_init()
        .expect("Failed initializing global subscriber");

    trace_me().await;

    // Ensures all traces finish getting exported
    opentelemetry::global::shutdown_tracer_provider();
}

#[tracing::instrument]
async fn trace_me() {
    for i in 0..2 {
        // 0,1
        do_work(i).await;
    }
}

#[tracing::instrument]
async fn do_work(arg: usize) {
    tracing::info!(iteration = arg);
    if arg == 1 {
        println!("sleeping...");
        tokio::time::sleep(Duration::from_secs(60)).await;
    }
}
