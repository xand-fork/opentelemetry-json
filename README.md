# opentelemetry-json

This repository is a crate for setting up Open Telemetry trace logging in a rust project.

## Tag-To-Publish Stable

Push a tag in the form of v${VERSION} to release a stable version of the crate.

## Beta Artifacts

All Development branches have manual jobs for publishing a beta package.
